import { Component } from '@angular/core';
import { DecimalPipe } from '@angular/common';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage {
  genders = [];
  hours = [];
  bottles = [];
  weight: number;
  gender: string;
  hour: number;
  bottle: number;
  promilles: number;

  constructor() {}

  ngOnInit() {
    this.genders.push('Male');
    this.genders.push('Female');

    //push hours and bottles into the array with a for loop for efficiency
    for (var i = 1;i < 25; i++) {
      this.hours.push(i);
    }

    for (var j = 0; j < 31; j++) {
      this.bottles.push(j);
    }

    this.gender = "Male";
    this.hour = 1;
    this.bottle = 0;
  }

  calculate() {
    let litres = this.bottle * 0.33;
    let grams = litres * 8 * 4.5;

    let burn = this.weight / 10;

    grams = grams - (burn * this.hour);

    if (this.gender === 'Male') {
      this.promilles = grams / (this.weight * 0.7);
    } else {
      this.promilles = grams / (this.weight * 0.6);
    }
  }

}
